#!/usr/bin/env node

const chalk = require('chalk')
const execa = require('execa')
const fs = require('fs')
const inquirer = require('inquirer')
const datepicker = require('inquirer-datepicker-prompt')
const moment = require('moment')
const boxen = require('boxen')

const noop = function () {}
const I = x => x

const contacts = [
  'TriConsulting',
  'Xcede',
  'AccessInsurance',
  'Healthily',
  'Christopher Cousins',
]

const clients = [
  'YNAP',
  'Accenture',
  'Avanade',
  'AccessInsurance',
  'Healthily',
  'Christopher Cousins',
]

const questions = [
  {
    name: 'contact',
    message: 'Who is the invoice for?',
    type: 'list',
    choices: contacts,
  },
  {
    name: 'client',
    message: 'Who is the client?',
    type: 'list',
    choices: clients,
  },
  {
    type: 'datetime',
    name: 'date',
    message: 'What is the week end date?',
    initial: new Date(),
    format: ['yyyy', ':', 'mm', ':', 'dd'],
  },
]

const output =
  parseable =>
  ({ stdout }) =>
    parseable
      ? console.log(stdout)
      : console.log(
          boxen(`${chalk.blue('The unique invoice number is: ')} ${stdout}`, {
            padding: 1,
            margin: 1,
            align: 'center',
            borderColor: 'yellow',
            borderStyle: 'round',
          })
        )

const execute = ({ contact, client, date, parseable }) => {
  const fdate = moment(date).format('YYYYMMDD')
  execa
    .shell(`echo -n '${contact}\n${client}\n${fdate}' | shasum | cut -c 1-6`)
    .then(output(parseable))
}

inquirer.registerPrompt('datetime', datepicker)

const yargs = require('yargs')
  .scriptName('invoice-sha')
  .usage(`$0 [args]`)
  .alias('c', 'contact')
  .describe('c', 'Who is the invoice for?')
  .choices('c', contacts)
  .default('c', contacts[0])
  .alias('t', 'client')
  .describe('t', 'Who is the client?')
  .choices('t', clients)
  .default('t', clients[0])
  .alias('d', 'date')
  .describe('d', 'What is the "week end" date (Fridays)?')
  .default('d', moment(new Date()).format('YYYY-MM-DD'))
  .command('$0', '', noop, noop)
  .alias('p', 'parseable')
  .describe('p', 'Show parseable output instead of fancy box.')
  .boolean('p')
  .help().argv

function defaultCommand({ contact, client, date, parseable }) {
  const options = [contact, client, date]
  const neededQuestions = options
    .map((option, i) => {
      if (!option) {
        return questions[i]
      }
    })
    .filter(I)

  if (options.length === 0) {
    return inquirer.prompt(questions).then(answers =>
      execute({
        ...answers,
        parseable,
      })
    )
  }

  if (options.length > 1) {
    return inquirer.prompt(neededQuestions).then(answers =>
      execute({
        contact: options[0],
        client: options[1],
        date: options[2],
        ...answers,
        parseable,
      })
    )
  }

  return execute({
    contact: options[0],
    client: options[1],
    date: options[2],
    parseable,
  })
}

if (require.main === module) {
  defaultCommand(yargs)
}

module.exports = {
  default: defaultCommand,
}
